var KF = {
    init: function(){
        clientWidth = document.body.clientWidth;
        clientHeight = document.body.clientHeight;
        if(clientWidth < 576) {
            $("body").addClass('touch-enabled');
            $("body").addClass('mobile');
        }
        KF.events();
    },
    events: function(){
        $(document).on("click", ".open-menu", function(){
            if($(this).hasClass("is-open")) {
                $("nav").removeClass("activate"); $(this).removeClass("is-open"); return;
            }
            $("nav").addClass("activate");
            $(this).addClass("is-open");
        })
    }
}